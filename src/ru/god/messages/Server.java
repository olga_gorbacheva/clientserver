package ru.god.messages;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Сервер ждет клиента...");

        try (Socket clientSocket = serverSocket.accept();
             DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
             DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream())) {

            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());
            String request;
            while (!(request = dataInputStream.readUTF()).equals("")) {
                System.out.println("прислал клиент: " + request);
                dataOutputStream.writeUTF(request);
                System.out.println("отправлено клиенту: " + request);
                dataOutputStream.flush();
            }
            System.out.println("клиент отключился");
        }
    }
}
