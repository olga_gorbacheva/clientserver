package ru.god.messages;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {
            System.out.println("В данной программе вы можете отправлять на сервер собщение и получать его обратно\n" +
                    "Чтобы прервать пересылку, оставьте строку пустой");
            String response = scanner.nextLine();
            dataOutputStream.writeUTF(response);

            while (!(response = dataInputStream.readUTF()).equals("")) {
                System.out.println("отправлено серверу: " + response);
                System.out.println("прислал сервер: " + response);
                response = scanner.nextLine();
                dataOutputStream.writeUTF(response);
                dataOutputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
